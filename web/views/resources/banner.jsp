<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="MyFirstTag" prefix="mytag" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/views/resources/style.css" />" >
</head>
<body>
<nav class="navbar navbar-inverse">
    <h2 class="topdiv">网上考勤系统
        <c:choose>
            <c:when test = "${not empty sessionScope.staff && not empty sessionScope.staff.username }">
                ，你好<c:out value="${staff.username}" />
            </c:when>
            <c:when test = "${not empty sessionScope.manager && not empty sessionScope.manager.username }">
                ，你好<c:out value="${manager.username}" />
            </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
    </h2>
    <p><mytag:date/></p>
</nav>



<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</body>
</html>