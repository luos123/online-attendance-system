<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>请假列表</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<div>
    <a href="<c:url value="/manager"/>"><input type="button" value="返回"></a>&nbsp;
    <a href="<c:url value="/manager/leaveReasons"/>"><input type="button" value="请假信息列表" style="width: 110px"></a>&nbsp;
    <br/><br/>
</div>
<div class="leaveList">
    <form method="post">
        <table>
            <tr>
                <th>ID</th>
                <th>员工名</th>
                <th>请假日期</th>
                <th>持续天数</th>
                <th>审核状态</th>
                <th>审核通过</th>
                <th>删除</th>
            </tr>
            <c:forEach items="${leave_list.items}" var="department">
                <tr id="staff_<c:out value="${department.id}"/>">
                    <td><c:out value="${department.id}"/></td>
                    <td><c:out value="${department.staff_username}"/></td>
                    <td><c:out value="${department.leavetime}"/></td>
                    <td><c:out value="${department.duration}"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${department.state eq 0}">
                                未审核
                            </c:when>
                            <c:when test="${department.state eq 1}">
                                已审核
                            </c:when>
                        </c:choose>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${department.state eq 0}">
                                <label>
                                    <input type="checkbox" name="approve" value="${department.id}"/>
                                </label>
                            </c:when>
                            <c:when test="${department.state eq 1}">
                                <label>
                                    <input type="checkbox" name="approve" disabled/>
                                </label>
                            </c:when>
                        </c:choose>
                    </td>
                    <td><label>
                        <input type="checkbox" name="delete" value="${department.id}">
                    </label></td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <input type="submit" value="执行"/>
    </form>
</div>
<br/><br/>
<div class="page">
    每页${leave_list.pageSize}个请假列表，
    第${leave_list.currentPageNo}/${leave_list.totalPageCount}页，共${leave_list.totalCount}个请假列表
    <c:if test="${leave_list.previousPage}">
        <c:choose>
            <c:when test="${leave_list.pageSize eq leave_list.PAGESIZE}">
                <a href="<c:url value="/manager/staffs?pageNo=${leave_list.currentPageNo - 1}" />">上一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/staffs?pageNo=${leave_list.currentPageNo - 1}&pageSize=${leave_list.pageSize}" />">上一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
    <c:if test="${leave_list.nextPage}">
        <c:choose>
            <c:when test="${leave_list.pageSize eq leave_list.PAGESIZE}">
                <a href="<c:url value="/manager/staffs?pageNo=${leave_list.currentPageNo + 1}" />">下一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/staffs?pageNo=${leave_list.currentPageNo + 1}&pageSize=${leave_list.pageSize}" />">下一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
</div>
</body>
</html>