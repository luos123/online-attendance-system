<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ page session="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>修改部门信息</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<h1>修改部门信息</h1>
<sf:form method="POST" commandName="department">
    <p>部门ＩＤ：<sf:input path="id"/> <sf:errors path="id" cssClass="error"/></p>
    <p>　部门名：<sf:input path="name"/> <sf:errors path="name" cssClass="error"/></p>
    <p>　父ＩＤ：<sf:input path="parentID"/> <sf:errors path="parentID" cssClass="error"/></p><br/>
    <input type="submit" value="修改"/>
</sf:form>
</body>
</html>