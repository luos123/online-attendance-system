<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ page session="false" %>
<html>
<head>
    <title>修改请假类型</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<h1>修改请假类型</h1>
<sf:form method="POST" commandName="reason">
    <p>ＩＤ：<sf:input path="id"/> <sf:errors path="id" cssClass="error"/></p>
    <p>类型：<sf:input path="reason"/> <sf:errors path="reason" cssClass="error"/></p><br/>
    <input type="submit" value="修改"/>
</sf:form>
</body>
</html>