<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>管理员界面</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<div class="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>

<body>

<a href="<c:url value="/manager/logout"/>">注销</a><br/>
<a href="<c:url value="/manager/staffs"/>">查看员工列表</a><br/>
<a href="<c:url value="/manager/departments"/>">查看部门列表</a><br/>
<a href="<c:url value="/manager/attendanceSheet"/>">查看考勤表</a><br/>
<a href="<c:url value="/manager/leaves"/>">查看请假列表</a><br/>

</body>

<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>
