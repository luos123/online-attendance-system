<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>考勤表</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<div>
    <a href="<c:url value="/manager"/>"><input type="button" value="返回"></a>
    <br/><br/>
</div>
<div>
    <form method="get">
        <label>
            选择日期
            <input type="date" name="date" value="${selectedDate}"/>
        </label>
        <input type="submit" value="确定">
    </form>
</div>
<div class="attendanceSheet">
    <table>
        <tr>
            <th>员工名</th>
            <th>状态</th>
            <th>签到时间</th>
        </tr>
        <c:forEach items="${sheets.items}" var="department">
            <tr id="staff_<c:out value="${department.id}"/>">
                <td><c:out value="${department.staff_username}"/></td>
                <c:choose>
                    <c:when test="${department.state eq 0}">
                        <td>未签到</td>
                        <td></td>
                    </c:when>
                    <c:when test="${department.state eq 1}">
                        <td>已签到</td>
                        <td><fmt:formatDate type="time" value="${department.attendancetime}"/></td>
                    </c:when>
                    <c:when test="${department.state eq 2}">
                        <td>请假</td>
                        <td></td>
                    </c:when>
                </c:choose>
            </tr>
        </c:forEach>
    </table>
    <br/>
</div>
<br/><br/>
<div class="page">
    每页${sheets.pageSize}项，
    第${sheets.currentPageNo}/${sheets.totalPageCount}页，共${sheets.totalCount}页
    <c:if test="${sheets.previousPage}">
        <c:choose>
            <c:when test="${sheets.pageSize eq sheets.PAGESIZE}">
                <a href="<c:url value="/manager/attendanceSheet?pageNo=${sheets.currentPageNo - 1}" />">上一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/attendanceSheet?pageNo=${sheets.currentPageNo - 1}&pageSize=${sheets.pageSize}" />">上一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
    <c:if test="${sheets.nextPage}">
        <c:choose>
            <c:when test="${sheets.pageSize eq sheets.PAGESIZE}">
                <a href="<c:url value="/manager/attendanceSheet?pageNo=${sheets.currentPageNo + 1}" />">下一页</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/manager/attendanceSheet?pageNo=${sheets.currentPageNo + 1}&pageSize=${sheets.pageSize}" />">下一页</a>
            </c:otherwise>
        </c:choose>
    </c:if>
</div>
</body>
</html>