<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ page session="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>添加请假信息</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/views/resources/style.css"/>">
</head>
<body>
<h1>添加请假信息</h1>
<sf:form method="POST" commandName="reason_new"> <sf:errors cssClass="error"/><br/>
    <p>　信息：<sf:input path="reason"/> <sf:errors path="reason" cssClass="error"/></p>
    <input type="submit" value="添加"/>
</sf:form>
<a href="<c:url value="/manager/leaveReasons"/>"><input type="button" value="返回"></a><br/>
</body>
</html>
