<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>主页</title>

    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/views/resources/style.css" />">
</head>
<div class="topdiv">
    <c:import url="/views/resources/banner.jsp"/>
</div>
<body>
<div class="choose">
    <c:out value="${staff.name}"/>
    <br/><br/>
    <a href="<c:url value="/staff/modifyinformation" />">修改个人信息</a> |
    <a href="<c:url value="/staff/attendancerecord" />">查看打卡记录</a> |
    <a href="<c:url value="/staff/leave" />">请假</a> |
    <a href="<c:url value="/staff/leaveList" />">请假列表</a> |
    <a href="<c:url value="/staff/logout" />">注销</a>
    <br/><br/>
    <form method="post">
        <input type="submit" value="签到" style="color: brown">
    </form>
    <c:choose>
        <c:when test="${flag == true}">
            <p>今日签到成功</p>
        </c:when>
        <c:when test="${flag ==false}">
            <p>不能重复签到</p>
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>
</div>
</body>
<div id="botdiv1">
    <c:import url="/views/resources/footer.jsp"/>
</div>
</html>