package attendance.web;

import attendance.client.Department;
import attendance.client.LeaveReason;
import attendance.client.ModifyStaff;
import attendance.client.Staff;
import attendance.db.ManagerRepository;
import attendance.system.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Date;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 管理员主页控制类
 *
 * @author 2018303010
 * @version 2.1
 */
@Controller // 控制定义
@RequestMapping("/manager") // 相应web路径
public class ManagerController {

    @Autowired
    private ManagerRepository managerRepository;

    @RequestMapping(method = GET)
    public String index() {
        return "system/index";
    }


    /**
     * 登录
     *
     * @param model   model
     * @param session session
     * @return 登录表单
     */
    @RequestMapping(value = "/login", method = GET)
    public String showLoginForm(Model model, HttpSession session) {
        session.removeAttribute("staff"); // 清除员工信息
        session.invalidate();
        model.addAttribute("today", new Date());
        return "system/loginForm";
    }

    /**
     * 登录请求
     *
     * @param userName 用户名
     * @param password 密码
     * @param session  session
     * @return 跳转的页面
     */
    @RequestMapping(value = "/login", method = POST)
    public String processLogin(@RequestParam(value = "username", defaultValue = "") String userName,
                               @RequestParam(value = "password", defaultValue = "") String password, HttpSession session) {
        Manager manager = null;
        try {
            manager = managerRepository.findByUserName(userName, password);
        } catch (DataAccessException ignored) { }
        if (manager != null) {
            session.setAttribute("manager", manager);
            return "redirect:/manager";
        } else {
            return "system/loginError";
        }
    }

    /**
     * 注销
     *
     * @param session session
     * @return 起始主页
     */
    @RequestMapping(value = "/logout", method = GET)
    public String logout(HttpSession session) {
        session.removeAttribute("manager");
        session.invalidate();
        return "redirect:/";
    }


    /**
     * 员工列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param model    模型
     * @return 要跳转的页面
     */
    @RequestMapping(value = "/staffs", method = GET)
    public String staffs(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, Model model) {
        if (pageSize <= 0) {
            pageSize = PaginationSupport.PAGESIZE;
        }
        try {
            model.addAttribute("staff_list", managerRepository.findStaffPage(pageNo, pageSize));
        } catch (DataAccessException ignored) { }
        return "system/staffs";
    }

    /**
     * 删除员工
     *
     * @param delete 要删除的ID
     */
    @RequestMapping(value = "/staffs", method = POST)
    public String deleteStaff(@RequestParam(value = "delete", required = false) String[] delete) {
        if (delete != null) {
            for (String i : delete) {
                try {
                    managerRepository.removeStaff(Long.parseLong(i));
                } catch (DataAccessException ignored) { }
            }
        }
        return "redirect:/manager/staffs";
    }

    /**
     * 增加员工
     *
     * @return 跳转页面
     */
    @RequestMapping(value = "/addStaff", method = GET)
    public String addStaff(Model model) {
        model.addAttribute("staff_new", new Staff());
        return "system/addStaff";
    }

    /**
     * 提交增加信息
     *
     * @param staffNew 新的员工信息
     * @param errors   错误
     * @return 跳转页面
     */
    @RequestMapping(value = "/addStaff", method = POST)
    public String addStaff(@Valid @ModelAttribute("staff_new") Staff staffNew, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "system/addStaff";
        }
        try {
            managerRepository.addStaff(staffNew);
        } catch (DataAccessException ignored) { }
        session.removeAttribute("staff_new");
        return "redirect:/manager";
    }

    /**
     * 修改员工信息
     */
    @RequestMapping(value = "/staff/{id}", method = GET)
    public String modifyStaff(@PathVariable("id") long staffID, Model model) {
        Staff staff = null;
        try {
            staff = managerRepository.queryStaff(staffID);
        } catch (DataAccessException ignored) { }
        model.addAttribute("staff", new ModifyStaff(staff.getName(), staff.getEmail(), staff.getPhoneNo(), staff.getAddress()));
        return "system/modifyStaff";
    }

    /**
     * 修改员工信息
     */
    @RequestMapping(value = "/staff/{id}", method = POST)
    public String modifyStaff(@PathVariable("id") long staffID,
                              @Valid @ModelAttribute("staff") ModifyStaff staff, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "system/modifyStaff";
        }
        try {
            managerRepository.modifyStaff(staffID, staff);
        } catch (DataAccessException ignored) { }
        session.removeAttribute("staff");
        return "redirect:/manager/staffs";
    }

    /**
     * 部门列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param model    模型
     * @return 跳转页面
     */
    @RequestMapping(value = "/departments", method = GET)
    public String departments(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                              @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, Model model) {
        if (pageSize <= 0) {
            pageSize = PaginationSupport.PAGESIZE;
        }
        try {
            model.addAttribute("department_list", managerRepository.findDepartmentPage(pageNo, pageSize));
        } catch (DataAccessException ignored) { }
        return "system/departments";
    }

    /**
     * 删除部门
     */
    @RequestMapping(value = "/departments", method = POST)
    public String deleteDepartment(@RequestParam(value = "delete", required = false) String[] delete) {
        if (delete != null) {
            for (String i : delete) {
                try {
                    managerRepository.removeDepartment(Long.parseLong(i));
                } catch (DataAccessException ignored) { }
            }
        }
        return "redirect:/manager/departments";
    }

    /**
     * 增加部门
     *
     * @return 跳转页面
     */
    @RequestMapping(value = "/addDepartment", method = GET)
    public String addDepartment(Model model) {
        model.addAttribute("department_new", new Department());
        return "system/addDepartment";
    }

    /**
     * 提交增加信息
     *
     * @param departmentNew 新信息
     * @param errors        错误
     * @return 跳转页面
     */
    @RequestMapping(value = "/addDepartment", method = POST)
    public String addDepartment(@Valid @ModelAttribute("department_new") Department departmentNew, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "system/addDepartment";
        }
        try {
            managerRepository.addDepartment(departmentNew);
        } catch (DataAccessException ignored) { }
        session.removeAttribute("department_new");
        return "redirect:/manager";
    }

    /**
     * 修改部门信息
     */
    @RequestMapping(value = "/department/{id}", method = GET)
    public String modifyDepartment(@PathVariable("id") long departmentID, Model model) {
        try {
            model.addAttribute("department", managerRepository.queryDepartment(departmentID));
        } catch (DataAccessException ignored) { }
        return "system/modifyDepartment";
    }

    /**
     * 修改部门信息
     */
    @RequestMapping(value = "/department/{id}", method = POST)
    public String modifyDepartment(@PathVariable("id") long departmentID,
                                   @Valid @ModelAttribute("department") Department department, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "system/modifyDepartment";
        }
        try {
            managerRepository.modifyDepartment(departmentID, department);
        } catch (DataAccessException ignored) { }
        session.removeAttribute("department");
        return "redirect:/manager/departments";
    }

    /**
     * 查询考勤表
     */
    @RequestMapping(value = "/attendanceSheet", method = GET)
    public String attendanceSheet(@RequestParam(value = "date", required = false) String date,
                                  @RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                                  @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, Model model) {
        int year, month, day;
        try {
            String[] d = date.split("-");
            year = Integer.parseInt(d[0]);
            month = Integer.parseInt(d[1]);
            day = Integer.parseInt(d[2]);
        } catch (Exception e) {
            Calendar cal = Calendar.getInstance();
            year = cal.get(Calendar.YEAR);
            month = cal.get(Calendar.MONTH) + 1;
            day = cal.get(Calendar.DATE);
            date = year + "-" + month + "-" + day;
        }

        model.addAttribute("selectedDate", date);
        if (pageSize <= 0) {
            pageSize = PaginationSupport.PAGESIZE;
        }
        try {
            model.addAttribute("sheets", managerRepository.querySheet(year, month, day, pageNo, pageSize));
        } catch (DataAccessException ignored) { }
        return "system/attendanceSheet";
    }


    /**
     * 请假列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param model    模型
     * @return 请假列表
     */
    @RequestMapping(value = "/leaves", method = GET)
    public String leaves(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, Model model) {
        if (pageSize <= 0) {
            pageSize = PaginationSupport.PAGESIZE;
        }
        try {
            model.addAttribute("leave_list", managerRepository.findLeavePage(pageNo, pageSize));
        } catch (DataAccessException ignored) { }
        return "system/leaves";
    }


    /**
     * 删除，审核请假
     */
    @RequestMapping(value = "/leaves", method = POST)
    public String deleteLeave(@RequestParam(value = "approve", required = false) String[] approve,
                              @RequestParam(value = "delete", required = false) String[] delete) {
        if (approve != null) {
            for (String i : approve) {
                try {
                    managerRepository.approveLeave(Long.parseLong(i));
                } catch (DataAccessException ignored) { }
            }
        }
        if (delete != null) {
            for (String i : delete) {
                try {
                    managerRepository.removeLeave(Long.parseLong(i));
                } catch (DataAccessException ignored) { }
            }
        }
        return "redirect:/manager/leaves";
    }

    /**
     * 请假信息列表
     *
     * @return 请假信息列表
     */
    @RequestMapping(value = "/leaveReasons", method = GET)
    public String leaveReasons(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, Model model) {
        if (pageSize <= 0) {
            pageSize = PaginationSupport.PAGESIZE;
        }
        try {
            model.addAttribute("leave_reason", managerRepository.findLeaveReasonPage(pageNo, pageSize));
        } catch (DataAccessException ignored) { }
        return "system/leaveReasons";
    }

    /**
     * 删除，审核请假
     */
    @RequestMapping(value = "/leaveReasons", method = POST)
    public String deleteLeaveReason(@RequestParam(value = "delete", required = false) String[] delete) {
        if (delete != null) {
            for (String i : delete) {
                try {
                    managerRepository.removeLeaveReason(Long.parseLong(i));
                } catch (DataAccessException ignored) { }
            }
        }
        return "redirect:/manager/leaveReasons";
    }

    /**
     * 修改请假信息
     */
    @RequestMapping(value = "/leaveReason/{id}", method = GET)
    public String modifyLeave(@PathVariable("id") long leaveID, Model model) {
        try {
            model.addAttribute("reason", managerRepository.queryLeave(leaveID));
        } catch (DataAccessException ignored) { }
        return "system/modifyLeave";
    }

    /**
     * 修改请假信息
     */
    @RequestMapping(value = "/leaveReason/{id}", method = POST)
    public String modifyLeave(@PathVariable("id") long reasonID,
                              @Valid @ModelAttribute("reason") LeaveReason reason, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "system/modifyLeave";
        }
        try {
            managerRepository.modifyLeaveReason(reasonID, reason);
        } catch (DataAccessException ignored) { }
        session.removeAttribute("reason");
        return "redirect:/manager/leaveReasons";
    }


    /**
     * 增加请假信息
     *
     * @return 请假信息
     */
    @RequestMapping(value = "/addReason", method = GET)
    public String addReason(Model model) {
        model.addAttribute("reason_new", new LeaveReason());
        return "system/addReason";
    }

    /**
     * 提交增加信息
     *
     * @param reasonNew 新信息
     * @param errors    错误
     * @return 页面
     */
    @RequestMapping(value = "/addReason", method = POST)
    public String addReason(@Valid @ModelAttribute("reason_new") LeaveReason reasonNew, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "system/addReason";
        }
        try {
            managerRepository.addReason(reasonNew);
        } catch (DataAccessException ignored) { }
        session.removeAttribute("reason_new");
        return "redirect:/manager/leaveReasons";
    }
}
