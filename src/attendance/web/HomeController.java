package attendance.web;

import attendance.db.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


/**
 * 系统主页控制类
 *
 * @author lx
 * @version v1.0
 */
@Controller // 控制定义
@RequestMapping("/") // 相应web路径
public class HomeController {

    @Autowired // 自动注入资源
    private StaffRepository staffRepository;

    /**
     * 用户首页
     *
     * @return 进入初始首页
     */
    @RequestMapping(method = GET) // 相应的请求方法
    public String home() {

        return "home";
    }

}
