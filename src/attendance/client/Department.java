package attendance.client;

import javax.validation.constraints.NotNull;

/**
 * 部门类
 *
 * @author wang sen
 * @version 1.0
 */
public class Department {
    /**
     * 部门ID
     */
    @NotNull
    private Long id;
    /**
     * 部门名称
     */
    @NotNull
    private String name;
    /**
     * 父部门ID
     */
    @NotNull
    private Long parentID;

    /**
     * 构造器
     *
     * @param id       部门ID
     * @param name     部门名称
     * @param parentID 父部门ID
     */
    public Department(Long id, String name, Long parentID) {
        super();
        this.id = id;
        this.name = name;
        this.parentID = parentID;
    }

    /**
     * 构造函数
     */
    public Department() {

    }

    /**
     * 获取部门ID
     *
     * @return id 部门ID
     */

    public Long getId() {
        return id;
    }

    /**
     * 设置部门ID
     *
     * @param id 部门ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取部门名称
     *
     * @return name 部门名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置部门名称
     *
     * @param name 部门名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取父部门ID
     *
     * @return parentID 父部门ID
     */
    public Long getParentID() {
        return parentID;
    }

    /**
     * 设置父部门ID
     *
     * @param parentID 父部门ID
     */
    public void setParentID(Long parentID) {
        this.parentID = parentID;
    }

}
