package attendance.client;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 用于修改员工信息的类
 *
 * @author wang sen
 * @version 1.0
 */
public class ModifyStaff {
    /**
     * 名字
     */
    @NotNull
    @Size(min = 1, max = 16)
    private String name;
    /**
     * 邮箱
     */
    @Email
    @NotNull
    private String email;
    /**
     * 手机号
     */
    @NotNull
    @Pattern(regexp = "^((0\\d{2,3}-\\d{7,8})|(1[34578]\\d{9}))$", message = "Wrong phone number")
    private String phoneNo;
    /**
     * 地址
     */
    @NotNull
    @Size(min = 1, max = 16)
    private String address;

    /**
     * 构造函数
     *
     * @param name    姓名
     * @param email   邮箱
     * @param phoneNo 手机号
     * @param address 地址
     */
    public ModifyStaff(String name, String email, String phoneNo, String address) {
        super();
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo;
        this.address = address;
    }

    /**
     * 构造函数
     */
    public ModifyStaff() {
    }

    /**
     * 获取名字
     *
     * @return name 名字
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名字
     *
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取邮箱
     *
     * @return email 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置邮箱
     *
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取电话号码
     *
     * @return 手机号
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * 设置手机号
     *
     * @param phoneNo 手机号
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * 获取地址
     *
     * @return 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置地址
     *
     * @param address 地址
     */
    public void setAddress(String address) {
        this.address = address;
    }


}
