package attendance.client;

/**
 * 请假原因类
 *
 * @author wang sen
 * @version 1.2
 */
public class LeaveReason {
    private Long id;
    /**
     * 请假人的用户名
     */
    private String reason;

    /**
     * 构造函数
     */
    public LeaveReason() {}

    /**
     * 构造函数
     *
     * @param id     请假原因ID
     * @param reason 请假原因
     */
    public LeaveReason(Long id, String reason) {
        super();
        this.id = id;
        this.reason = reason;
    }

    /**
     * 获取ID
     *
     * @return id ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置Id
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取请假原因
     *
     * @return reason 请假原因
     */
    public String getReason() {
        return reason;
    }

    /**
     * 设置请假原因
     *
     * @param reason 请假原因
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
}
