package attendance.client;

import java.util.Date;

/**
 * 请假类
 *
 * @author wang sen
 * @version 1.0
 */

public class Leave {
    private Long id;
    /**
     * 请假人的用户名
     */
    private String staff_username;
    /**
     * 请假的起始时间
     */
    private Date leavetime;
    /**
     * 请假时长
     */
    private int Duration;
    /**
     * 0是正在审核，1是已经审核通过，2是销假，3是已经过期 （暂时这么安排）
     */
    private int state;
    /**
     * 审核人管理员的用户名，可为空
     */
    private String manager_username;
    /**
     * 请假原因
     */
    private String reason;


    /**
     * 构造函数
     *
     * @param id               请假ID
     * @param staff_username   请假的员工姓名
     * @param leavetime        请假的起始时间
     * @param duration         请假时长
     * @param state            请假的状态 0为正在审核，1为通过审核，2为销假，3为已过期
     * @param manager_username 审核人用户名
     * @param reason           请假原因
     */
    public Leave(Long id, String staff_username, Date leavetime, int duration, int state, String manager_username,
                 String reason) {
        super();
        this.id = id;
        this.staff_username = staff_username;
        this.leavetime = leavetime;
        Duration = duration;
        this.state = state;
        this.manager_username = manager_username;
        this.reason = reason;
    }

    /**
     * 获取请假原因
     *
     * @return reason 请假原因
     */
    public String getReason() {
        return reason;
    }

    /**
     * 设置请假原因
     *
     * @param reason 请假原因
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * 获取请假ID
     *
     * @return id 请假ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置请假ID
     *
     * @param id 请假ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取请假员工姓名
     *
     * @return staff_username 请假员工姓名
     */
    public String getStaff_username() {
        return staff_username;
    }

    /**
     * 设置请假员工姓名
     *
     * @param staff_username 请假的员工姓名
     */
    public void setStaff_username(String staff_username) {
        this.staff_username = staff_username;
    }

    /**
     * 获取请假起始时间
     *
     * @return leavetime 请假起始时间
     */
    public Date getLeavetime() {
        return leavetime;
    }

    /**
     * 设置请假起始时间
     *
     * @param leavetime 请假起始时间
     */
    public void setLeavetime(Date leavetime) {
        this.leavetime = leavetime;
    }

    /**
     * 获取请假时长
     *
     * @return Duration 请假时长
     */
    public int getDuration() {
        return Duration;
    }

    /**
     * 设置请假时长
     *
     * @param duration 请假时长
     */
    public void setDuration(int duration) {
        Duration = duration;
    }

    /**
     * 获取请假的状态
     *
     * @return state 请假状态
     */
    public int getState() {
        return state;
    }

    /**
     * 设置请假状态
     *
     * @param state 请假状态
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * 获取审核人姓名
     *
     * @return manager_username 审核人姓名
     */
    public String getManager_username() {
        return manager_username;
    }

    /**
     * 设置审核人姓名
     *
     * @param manager_username 审核人姓名
     */
    public void setManager_username(String manager_username) {
        this.manager_username = manager_username;
    }


}
