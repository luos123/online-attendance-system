package attendance.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class ErrorTag extends SimpleTagSupport {

    /**
     * 实现doTag方法
     *
     * @throws JspException Jsp页面异常
     */
    @Override
    public void doTag() throws JspException {
//        this.getJspBody
        try {
            JspWriter out = getJspContext().getOut();
            JspFragment frag = getJspBody();


            frag.invoke(null);
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
    }
}
