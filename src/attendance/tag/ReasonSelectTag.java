package attendance.tag;

import attendance.client.LeaveReason;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.util.List;

/**
 * 下拉列表的Tag
 *
 * @author wang sen
 * @version 1.0
 */
public class ReasonSelectTag extends SimpleTagSupport {

    /**
     * 实现doTag方法
     *
     * @throws JspException Jsp页面异常
     */
    @Override
    public void doTag() throws JspException {
        try {
            JspWriter out = getJspContext().getOut();
            ServletRequest req = ((PageContext) getJspContext()).getRequest();
            System.out.println("leavereason:" + req.getAttribute("leavereason"));
            List<LeaveReason> str = (List<LeaveReason>) req.getAttribute("leavereason");

            StringBuilder outPrint = new StringBuilder();
            outPrint.append("<select name='reason' >");
            for (LeaveReason reason : str) {
                outPrint.append("<option>");
                outPrint.append(reason.getReason());
                outPrint.append("</option>");
            }
            outPrint.append("</select>");
            out.print(outPrint.toString());
        } catch (java.io.IOException e) {
            throw new JspTagException(e.getMessage());
        }

    }
}