package attendance.db;

import attendance.client.*;
import attendance.web.PaginationSupport;

import java.util.Date;
import java.util.List;

/**
 * 管理员资源库接口
 *
 * @author wang sen
 * @version 1.0
 */
public interface StaffRepository {
    /**
     * 员工注册
     *
     * @param staff 员工
     */
    void addStaff(Staff staff);

    /**
     * 查找员工
     *
     * @param userName 用户名
     * @param password 密码
     * @return 返回员工
     */
    Staff findByUserName(String userName, String password);

    /**
     * 添加员工的打卡信息
     *
     * @param uername 员工用户名
     * @return 是否添加成功（一天只能打一次卡）
     */
    boolean addAttendance(String uername);

    /**
     * 更新用户信息
     *
     * @param staffInfo 员工信息
     * @param id        ID
     */
    void modify(ModifyStaff staffInfo, Long id);

    /**
     * 查找员工
     *
     * @param id 员工ID
     * @return 员工
     */
    Staff findByUserName(Long id);

    /**
     * 用户出勤分页查看
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param username 用户名
     * @return 用户出勤分页
     */
    PaginationSupport<Attendance> attendancePage(int pageNo, int pageSize, String username);

    /**
     * 添加请假到数据库中
     *
     * @param leavedate 请假日期
     * @param leavetime 请假时长
     * @param reason    请假原因
     * @param username  用户名
     */
    void addLeave(Date leavedate, int leavetime, String reason, String username);

    /**
     * 某个用户的请假分页查看
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @param username 用户名
     * @return 请假分页
     */
    PaginationSupport<Leave> leavePage(int pageNo, int pageSize, String username);

    /**
     * 从数据库中查看请假事由
     *
     * @return 请假事由
     */
    List<LeaveReason> getLeaveReason();

}
