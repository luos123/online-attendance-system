package attendance.db;

import attendance.client.*;
import attendance.system.Manager;
import attendance.web.PaginationSupport;

import java.util.Date;


/**
 * 管理员资源库接口
 *
 * @author 2018303010
 * @version 1.5
 */
public interface ManagerRepository {
    /**
     * 添加员工
     *
     * @param staff 员工
     */
    void addStaff(Staff staff);

    /**
     * 删除员工
     *
     * @param id 员工id
     */
    void removeStaff(long id);

    /**
     * 删除员工
     *
     * @param username 员工用户名
     */
    void removeStaff(String username);

    /**
     * 修改员工信息
     *
     * @param id    员工id
     * @param staff 新信息
     */
    void modifyStaff(long id, ModifyStaff staff);

    /**
     * 查询员工信息
     *
     * @param id 员工id
     * @return 信息
     */
    Staff queryStaff(long id);

    /**
     * 添加部门
     *
     * @param department 部门
     */
    void addDepartment(Department department);

    /**
     * 删除部门信息
     *
     * @param id 部门id
     */
    void removeDepartment(long id);

    /**
     * 修改部门信息
     *
     * @param id         部门id
     * @param department 新信息
     */
    void modifyDepartment(long id, Department department);

    /**
     * 查询部门信息
     *
     * @param id 部门id
     * @return 信息
     */
    Department queryDepartment(long id);

    /**
     * 修改请假信息
     *
     * @param id     id
     * @param reason 新信息
     */
    void modifyLeaveReason(long id, LeaveReason reason);

    /**
     * 查询统计指定日期范围的考勤表
     *
     * @param date     日期
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     */
    PaginationSupport<Attendance> querySheet(Date date, int pageNo, int pageSize);

    /**
     * 查询统计指定日期范围的考勤表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 请假信息列表
     */
    PaginationSupport<Attendance> querySheet(int year, int month, int date, int pageNo, int pageSize);

    /**
     * 依据页码和指定页面大小，返回员工列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 分页对象
     */
    PaginationSupport<Staff> findStaffPage(int pageNo, int pageSize);

    /**
     * 依据用户名（登录名），密码查找管理员
     *
     * @param userName 用户名（登录名）
     * @param password 密码
     * @return 管理员
     */
    Manager findByUserName(String userName, String password);

    /**
     * 依据页码和指定页面大小，返回部门列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 分页对象
     */
    PaginationSupport<Department> findDepartmentPage(int pageNo, int pageSize);

    /**
     * 依据页码和指定页面大小，返回请假列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 分页对象
     */
    PaginationSupport<Leave> findLeavePage(int pageNo, int pageSize);

    /**
     * 查找请假类型
     *
     * @return 请假类型
     */
    LeaveReason queryLeave(long id);

    /**
     * 通过请假
     *
     * @param id id
     */
    void approveLeave(long id);

    /**
     * 删除请假
     *
     * @param id id
     */
    void removeLeave(long id);

    /**
     * 依据页码和指定页面大小，返回请假信息列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 请假信息列表
     */
    PaginationSupport<LeaveReason> findLeaveReasonPage(int pageNo, int pageSize);

    /**
     * 删除请假信息
     *
     * @param id id
     */
    void removeLeaveReason(long id);

    /**
     * 添加请假信息
     *
     * @param reason 信息
     */
    void addReason(LeaveReason reason);


}
