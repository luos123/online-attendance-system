package attendance.db.jdbc;

import attendance.client.*;
import attendance.db.ManagerRepository;
import attendance.system.Manager;
import attendance.web.PaginationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 管理员资源库实现类
 *
 * @author 2018303010
 * @version 1.5
 */
@Repository
public class JdbcManagerRepository implements ManagerRepository {
    private final JdbcTemplate jdbc;

    @Autowired
    public JdbcManagerRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    /**
     * 部门类RowMapper
     */
    protected static class DepartmentRowMapper implements RowMapper<Department> {
        @Override
        public Department mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Department(rs.getLong("ID"), rs.getString("name"), rs.getLong("parent_ID"));
        }
    }

    /**
     * 管理员类RowMapper
     */
    protected static class ManagerRowMapper implements RowMapper<Manager> {
        @Override
        public Manager mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Manager(rs.getLong("ID"), rs.getString("username"), rs.getString("password"), rs.getString("name"), rs.getString("email"));
        }
    }

    /**
     * 添加员工
     *
     * @param staff 员工
     */
    @Override
    public void addStaff(Staff staff) {
        jdbc.update("insert into staff (username, password, name, email, phoneNo, department_id, address) values (?,?,?,?,?,?,?)", staff.getUsername(), staff.getPassword(), staff.getName(), staff.getEmail(), staff.getPhoneNo(), staff.getDepartment_id(), staff.getAddress());
    }

    /**
     * 删除员工
     *
     * @param id 员工id
     */
    @Override
    public void removeStaff(long id) {
        jdbc.update("delete from staff where ID=?", id);
    }

    /**
     * 删除员工
     *
     * @param username 员工用户名
     */
    @Override
    public void removeStaff(String username) {
        jdbc.update("delete from staff where username=?", username);
    }

    /**
     * 修改员工信息
     *
     * @param id    员工id
     * @param staff 新信息
     */
    @Override
    public void modifyStaff(long id, ModifyStaff staff) {
        jdbc.update("update staff set name=?,email=?,phoneNo=?,address=? where ID=?", staff.getName(), staff.getEmail(), staff.getPhoneNo(), staff.getAddress(), id);
    }

    /**
     * 查询员工信息
     *
     * @param id 员工id
     * @return 信息
     */
    @Override
    public Staff queryStaff(long id) {
        Staff staff;
        staff = jdbc.queryForObject("select * from staff where ID=?", new JdbcStaffRepository.StaffRowMapper(), id);
        return staff;
    }

    /**
     * 添加部门
     *
     * @param department 部门
     */
    @Override
    public void addDepartment(Department department) {
        jdbc.update("insert into department values (?,?,?)", department.getId(), department.getName(), department.getParentID());
    }

    /**
     * 删除部门信息
     *
     * @param id 部门id
     */
    @Override
    public void removeDepartment(long id) {
        jdbc.update("delete from department where ID=?", id);
    }

    /**
     * 修改部门信息
     *
     * @param id         部门id
     * @param department 新信息
     */
    @Override
    public void modifyDepartment(long id, Department department) {
        jdbc.update("update department set ID=?,name=?,parent_ID=? where ID=?", department.getId(), department.getName(), department.getParentID(), id);
    }

    /**
     * 查询部门信息
     *
     * @param id 部门id
     * @return 信息
     */
    @Override
    public Department queryDepartment(long id) {
        Department department;
        department = jdbc.queryForObject("select * from department where ID=?", new DepartmentRowMapper(), id);
        return department;
    }

    /**
     * 查找请假类型
     *
     * @param id ID
     * @return 请假类型
     */
    @Override
    public LeaveReason queryLeave(long id) {
        LeaveReason leaveReason;
        leaveReason = jdbc.queryForObject("select * from leavereason where ID=?", new JdbcStaffRepository.LeaveReasonRowMapper(), id);
        return leaveReason;
    }

    /**
     * 修改请假信息
     *
     * @param id     id
     * @param reason 新信息
     */
    @Override
    public void modifyLeaveReason(long id, LeaveReason reason) {
        jdbc.update("update leavereason set reason=? where ID=?", reason.getReason(), id);
    }

    /**
     * 查询统计指定日期范围的考勤表
     *
     * @param date     日期
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 考勤表
     */
    @Override
    public PaginationSupport<Attendance> querySheet(Date date, int pageNo, int pageSize) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        List<Attendance> items = jdbc.query("select * from attendance where year = ? and month = ? and day = ? order by attendanceTime limit ? offset ?", new JdbcStaffRepository.AttendanceRowMapper(), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE), pageSize, startIndex);
        int totalCount = items.size();
        if (totalCount < 1) { return new PaginationSupport<>(new ArrayList<>(0), 0);}
        return new PaginationSupport<>(items, totalCount, pageSize, startIndex);
    }

    /**
     * 查询统计指定日期范围的考勤表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 请假信息列表
     */
    @Override
    public PaginationSupport<Attendance> querySheet(int year, int month, int date, int pageNo, int pageSize) {
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        List<Staff> staffList = jdbc.query("select * from staff order by id limit ? offset ?", new JdbcStaffRepository.StaffRowMapper(), pageSize, startIndex);
        List<Attendance> items = jdbc.query("select * from attendance where year = ? and month = ? and day = ? order by attendanceTime limit ? offset ?", new JdbcStaffRepository.AttendanceRowMapper(), year, month, date, pageSize, startIndex);
        for (Attendance i : items) {
            staffList.removeIf(staff -> staff.getUsername().equals(i.getStaff_username()));
        }
        for (Staff i : staffList) {
            items.add(new Attendance(i.getName(), 0));
        }
        int totalCount = items.size();
        if (totalCount < 1) { return new PaginationSupport<>(new ArrayList<>(0), 0);}
        return new PaginationSupport<>(items, totalCount, pageSize, startIndex);
    }

    /**
     * 依据页码和指定页面大小，返回员工列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 分页对象
     */
    @Override
    public PaginationSupport<Staff> findStaffPage(int pageNo, int pageSize) {
        int totalCount = jdbc.queryForObject("select count(id) from manager", Integer.class);
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        if (totalCount < 1) { return new PaginationSupport<>(new ArrayList<>(0), 0);}
        List<Staff> items = jdbc.query("select * from staff order by id limit ? offset ?", new JdbcStaffRepository.StaffRowMapper(), pageSize, startIndex);
        return new PaginationSupport<>(items, totalCount, pageSize, startIndex);
    }

    /**
     * 依据用户名（登录名），密码查找管理员
     *
     * @param username 用户名（登录名）
     * @param password 密码
     * @return 管理员
     */
    @Override
    public Manager findByUserName(String username, String password) {
        Manager manager = null;
        try {
            manager = jdbc.queryForObject("select * from manager where username = ? and password = ?", new ManagerRowMapper(), username, password);
        } catch (DataAccessException ignored) {
        }
        return manager;
    }

    /**
     * 依据页码和指定页面大小，返回部门列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 分页对象
     */
    @Override
    public PaginationSupport<Department> findDepartmentPage(int pageNo, int pageSize) {
        int totalCount = jdbc.queryForObject("select count(id) from department", Integer.class);
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        if (totalCount < 1) { return new PaginationSupport<>(new ArrayList<>(0), 0);}
        List<Department> items = jdbc.query("select * from department order by id limit ? offset ?", new DepartmentRowMapper(), pageSize, startIndex);
        return new PaginationSupport<>(items, totalCount, pageSize, startIndex);
    }

    /**
     * 依据页码和指定页面大小，返回请假列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 分页对象
     */
    @Override
    public PaginationSupport<Leave> findLeavePage(int pageNo, int pageSize) {
        int totalCount = jdbc.queryForObject("select count(id) from `leave`", Integer.class);
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        if (totalCount < 1) { return new PaginationSupport<>(new ArrayList<>(0), 0);}
        List<Leave> items = jdbc.query("select * from `leave` order by id limit ? offset ?", new JdbcStaffRepository.LeaveRowMapper(), pageSize, startIndex);
        return new PaginationSupport<>(items, totalCount, pageSize, startIndex);
    }

    /**
     * 依据页码和指定页面大小，返回请假信息列表
     *
     * @param pageNo   起始位置
     * @param pageSize 每页数量
     * @return 分页对象
     */
    @Override
    public PaginationSupport<LeaveReason> findLeaveReasonPage(int pageNo, int pageSize) {
        int totalCount = jdbc.queryForObject("select count(id) from leavereason", Integer.class);
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        if (totalCount < 1) { return new PaginationSupport<>(new ArrayList<>(0), 0);}
        List<LeaveReason> items = jdbc.query("select * from leavereason order by id limit ? offset ?", new JdbcStaffRepository.LeaveReasonRowMapper(), pageSize, startIndex);
        return new PaginationSupport<>(items, totalCount, pageSize, startIndex);
    }

    /**
     * 通过请假
     *
     * @param id id
     */
    @Override
    public void approveLeave(long id) {
        jdbc.update("update `leave` set state=1 where ID=?", id);
    }

    /**
     * 删除请假
     *
     * @param id id
     */
    @Override
    public void removeLeave(long id) {
        jdbc.update("delete from `leave` where ID=?", id);
    }

    /**
     * 删除请假信息
     *
     * @param id id
     */
    @Override
    public void removeLeaveReason(long id) {
        jdbc.update("delete from leavereason where ID=?", id);
    }

    /**
     * 添加请假信息
     *
     * @param reason 信息
     */
    @Override
    public void addReason(LeaveReason reason) {
        jdbc.update("insert into leavereason (reason) values (?)", reason.getReason());
    }
}
