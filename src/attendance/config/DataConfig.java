package attendance.config;

import com.mysql.cj.jdbc.Driver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 数据库配置
 *
 * @author 2018303010
 * @version 1.2
 */
@Configuration
public class DataConfig {
    /**
     * 数据源设置
     *
     * @return DataSource
     */
    @Bean
    public DataSource dataSource() throws SQLException {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        Properties properties = new Properties();
        dataSource.setDriver(new Driver());

        dataSource.setUsername("root");
        dataSource.setPassword("123456");

        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/test");
        properties.setProperty("serverTimezone", "Asia/Shanghai");
        properties.setProperty("characterEncoding", "utf8");
        properties.setProperty("useSSL", "false");
        properties.setProperty("rewriteBatchedStatements", "true");
        properties.setProperty("allowPublicKeyRetrieval", "true");
        dataSource.setConnectionProperties(properties);
        return dataSource;
    }

    /**
     * jdbc模板配置，采用spring默认的JdbcTemplate模板
     *
     * @param dataSource 数据源
     * @return jdbc模板
     */
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
